from django.core import serializers
from rest_framework import serializers
from contributors import models as cmodels
from signon.models import Identity

class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)

    class Meta:
        model = cmodels.User
        fields = ["id", "username", "full_name", "is_dd", "is_staff",
                "is_active"]

class IdentifierSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)

    class Meta:
        model = cmodels.Identifier
        fields = ["id", "type", "name", "user"]

class IdentitySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)
    
    class Meta:
        model = Identity
        fields = ["id", "person", "issuer", "subject", "profile", "picture",
                "fullname", "username"]

    def create(self, validated_data):
        identity = Identity(**validated_data)
        identity.save(audit_skip=True)
        return identity

class ContributionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)
    type_name = serializers.CharField(source="type.name")
    type_source = serializers.CharField(source="type.source.name")

    class Meta:
        model = cmodels.Contribution
        fields = ["id", "identifier", "begin", "until", "url", "type_name",
                "type_source"]

    def create(self, validated_data):
        type = validated_data.pop("type")
        contribution = cmodels.Contribution(**validated_data)
        contribution.type = cmodels.ContributionType.objects.get(
                name=type["name"], source__name=type["source"]["name"])
        contribution.save()
        return contribution
