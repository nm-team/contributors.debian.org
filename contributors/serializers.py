from rest_framework import serializers
from contributors import models as cmodels


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = cmodels.User
        fields = ('username', 'full_name')


class ContributorSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = cmodels.AggregatedPerson
        fields = ('id', 'user', 'begin', 'until')


class IdentifierSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = cmodels.Identifier
        fields = ("type", "name", "user")
