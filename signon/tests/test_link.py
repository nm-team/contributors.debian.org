from __future__ import annotations
from django.test import TestCase, override_settings
from django.urls import reverse
from signon.unittest import SignonFixtureMixin
from signon import providers


@override_settings(SIGNON_PROVIDERS=[
    providers.DebssoProvider(name="debsso", label="sso.debian.org"),
    providers.BaseSessionProvider(name="salsa", label="Salsa", single_bind=True),
])
class TestLink(SignonFixtureMixin, TestCase):
    def test_link(self):
        self.identities.create("user1", person=self.user1, issuer="debsso", subject="user@debian.org", audit_skip=True)
        self.identities.create("user2", person=self.user2, issuer="salsa", subject="2", audit_skip=True)

        client = self.make_test_client(None, [
            self.identities.user1,
            self.identities.user2,
        ])

        with self.assertLogs() as log:
            response = client.get(reverse('signon:link', args=["debsso", self.user2.pk]))
        self.assertEqual(log.output, [
            "ERROR:signon.middleware:Conflicting person mapping: "
            f"identities ({self.identities.user1}, {self.identities.user2})"
            f" map to at least {self.user1} and {self.user2}",
        ])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["provider"].name, "debsso")
        self.assertEqual(response.context["identity"], self.identities.user1)
        self.assertEqual(response.context["target"], self.user2)
        self.assertTrue(response.context["is_last"])

        with self.assertLogs() as log:
            response = client.post(reverse('signon:link', args=["debsso", self.user2.pk]))
        self.assertEqual(log.output, [
            "ERROR:signon.middleware:Conflicting person mapping: "
            f"identities ({self.identities.user1}, {self.identities.user2})"
            f" map to at least {self.user1} and {self.user2}",
        ])
        self.assertRedirects(response, reverse("signon:login"))

        self.identities.user1.refresh_from_db()
        self.assertEqual(self.identities.user1.person, self.user2)

    def test_link_unrelated_user(self):
        self.identities.create("user1", person=self.user1, issuer="debsso", subject="user@debian.org", audit_skip=True)
        self.identities.create("user2", person=self.user2, issuer="salsa", subject="2", audit_skip=True)

        client = self.make_test_client(None, [
            self.identities.user1,
        ])

        response = client.get(reverse('signon:link', args=["debsso", self.user2.pk]))
        self.assertPermissionDenied(response)

        response = client.post(reverse('signon:link', args=["debsso", self.user2.pk]))
        self.assertPermissionDenied(response)

        self.identities.user1.refresh_from_db()
        self.assertEqual(self.identities.user1.person, self.user1)

    def test_unlink(self):
        self.identities.create("user1", person=self.user1, issuer="debsso", subject="user@debian.org", audit_skip=True)
        self.identities.create("user2", person=self.user1, issuer="salsa", subject="2", audit_skip=True)

        client = self.make_test_client(None, [
            self.identities.user1,
            self.identities.user2,
        ])

        response = client.get(reverse('signon:unlink', args=["debsso"]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["provider"].name, "debsso")
        self.assertEqual(response.context["identity"], self.identities.user1)
        self.assertFalse(response.context["is_last"])

        response = client.post(reverse('signon:unlink', args=["debsso"]))
        self.assertRedirects(response, reverse("signon:login"))

        self.identities.user1.refresh_from_db()
        self.assertIsNone(self.identities.user1.person)
        self.identities.user2.refresh_from_db()
        self.assertEqual(self.identities.user2.person, self.user1)

    def test_unlink_unrelated(self):
        self.identities.create("user1", person=self.user1, issuer="debsso", subject="user@debian.org", audit_skip=True)
        self.identities.create("user2", person=self.user1, issuer="salsa", subject="2", audit_skip=True)

        client = self.make_test_client(None, [
            self.identities.user1,
        ])

        response = client.get(reverse('signon:unlink', args=["salsa"]))
        self.assertEqual(response.status_code, 404)

        response = client.post(reverse('signon:unlink', args=["salsa"]))
        self.assertEqual(response.status_code, 404)
