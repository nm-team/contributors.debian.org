from django.contrib import admin
from .models import Identity


@admin.register(Identity)
class IdentityAdmin(admin.ModelAdmin):
    # these field names are specific to contributors.models.User
    search_fields = (
        "username", "fullname", "subject",
        "person__full_name", "person__username",
    )

    def save_model(self, request, obj, form, change):
        """
        Given a model instance save it to the database.
        """
        obj.save(audit_author=request.user, audit_notes="edited from admin")
