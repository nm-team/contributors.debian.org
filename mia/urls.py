from django.urls import path
from . import views

urlpatterns = [
    path('last-significant.json', views.ByFingerprint.as_view(), name="mia_last_significant"),
]
