from __future__ import annotations
from django.core.exceptions import ImproperlyConfigured
from django.contrib.auth import get_user_model


class ImpersonateMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.User = get_user_model()

    def __call__(self, request):
        # AuthenticationMiddleware is required so that request.user exists.
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured(
                "The impersonator middleware requires the authentication middleware"
                " to be installed.  Edit your MIDDLEWARE setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the ImpersonateMiddleware class.")

        if request.user.is_authenticated:
            # Implement impersonation if requested in session
            if request.user.is_staff:
                pk = request.session.get("impersonate", None)
                if pk is not None:
                    try:
                        user = self.User.objects.get(pk=pk)
                    except self.User.DoesNotExist:
                        user = None

                    if user is not None:
                        request.impersonator = request.user
                        request.user = user

        return self.get_response(request)
