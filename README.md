Debian Contributors web application
===================================

See https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel for this project's page in the Debian wiki.

## Running this code on your own machine

### Dependencies

    apt-get install python3-debiancontributors \
                    python3-django-housekeeping \
                    python3-django \
                    python3-djangorestframework \
                    python3-git \
                    python3-gitlab \
                    python3-model-bakery \
                    libjs-bootstrap4 \
                    fonts-fork-awesome \
                    libjs-jquery-datatables \
                    eatmydata

      (eatmydata is not necessary but it will make everything run much faster 
       in a test environment)

For a more up-to-date list of dependencies, refer to the `.gitlab-ci.yml` file. 

### Configuration

    cp dc/local_settings.py.devel dc/local_settings.py
    edit dc/local_settings.py as needed

### First setup

    ./manage.py migrate


### Fill in data

Visit http://contributors.debian.org/contributors/export/public to export
the production data in json format.

> Currently only a DD user has access to this URL. Also email and other
sensitive information will be either replaced with dummy ones or missing.

Import the data into your environment by running the following command:

```
eatmydata ./manage.py import_data exported_data.json
```

To import only the sources, import the information from
http://contributors.debian.org/contributors/export/sources.
if you are not logged as a DD, authentication tokens are replaced with dummy
ones:

    curl https://contributors.debian.org/contributors/export/sources | eatmydata ./manage.py import_sources

    eatmydata ./manage.py runserver

From now on, imported sources will be available from http://localhost:8000/source/. 

You may login to http://localhost:8000/admin/ which grants access to the sources and other tables. (currently broken)

Go to https://contributors.debian.org/source/, choose a data source, click on
"Members", then "Add me as member": you'll then be able to access the data
source configuration. From there you can see the data source name and the auth
token for posting.

Acquire some JSON data (ask Enrico at the moment) and import it:

    dc-tool --post --baseurl=http://localhost:8000 --source sourcename --auth-token sourcetoken datafile.json

As a test, to quickly generate some test data, run the following command:

```bash
eatmydata ./manage.py generate_test_data
```

### Run housekeeping

Create user accounts from your datafile submission (API key from regular salsa users won't be autorized to perform this task):

    eatmydata ./manage.py housekeeping

### Run the web server

    eatmydata ./manage.py runserver

## Development

Development targets the current Debian Stable plus backports, and that
determines the version of Django and the dependencies that can be used.

You can find a TODO list at https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel

## Authentication

Authentication in production is meant to be performed by Apache, so the
application relies on RemoteUserMiddleware, taking the user name from the
`SSL_CLIENT_S_DN_CN` env var so that it comes from Debian Single Signon certificates.

For testing purposes, you can set `settings.TEST_USER` to override
`SSL_CLIENT_S_DN_CN`.

