from __future__ import annotations
from django.test import TestCase
from django.urls import reverse
from dc.unittest import SourceFixtureMixin


class TestContributionTypeUpdate(SourceFixtureMixin, TestCase):
    def test_same_name(self):
        client = self.make_test_client("admin")
        response = client.get(reverse("sources:ctype_update", kwargs={"sname": "test", "name": "tester"}))
        self.assertEqual(response.status_code, 200)
