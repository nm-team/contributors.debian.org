from __future__ import annotations
from django.urls import path
from . import views

app_name = "sources"

urlpatterns = [
    path('', views.SourcesList.as_view(), name="list"),
    path('add/', views.SourceCreate.as_view(), name='add'),
    path('<sname>/', views.SourceView.as_view(), name='view'),
    path('<sname>/ctype/<name>/', views.CtypeView.as_view(), name='ctype_view'),
    path('<sname>/update/', views.SourceUpdate.as_view(), name='update'),
    path('<sname>/delete/', views.SourceDelete.as_view(), name='delete'),
    path('<sname>/delete/contributions/', views.SourceDeleteContributions.as_view(), name='delete_contributions'),
    path('<sname>/backups/', views.SourceBackupList.as_view(), name='backup_list'),
    path('<sname>/backups/<int:backup_id>/', views.SourceBackupDownload.as_view(), name='backup_download'),
    path('<sname>/members/', views.SourceMembers.as_view(), name='members'),
    path('<sname>/members/add/', views.SourceMembersAdd.as_view(), name='members_add'),
    path('<sname>/members/delete/', views.SourceMembersDelete.as_view(), name='members_delete'),
    path('<sname>/ctypes/add/', views.ContributionTypeCreate.as_view(), name='ctype_add'),
    path('<sname>/ctype/<name>/update/', views.ContributionTypeUpdate.as_view(), name='ctype_update'),
    path('<sname>/ctype/<name>/delete/', views.ContributionTypeDelete.as_view(), name='ctype_delete'),
    path('<sname>/ctype/<name>/delete/contributions/', views.ContributionTypeDeleteContributions.as_view(),
         name='ctype_delete_contributions'),
    path('<sname>/user-settings/', views.SourceUserSettings.as_view(), name='user_settings'),
]
