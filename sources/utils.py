import os
import re
import datetime
from collections import defaultdict


re_fname = re.compile(r"^\d{8}[.-]")


def list_backup_files(path):
    """
    List all backup files in a path
    """
    for dirpath, dirnames, filenames, dirfd in os.fwalk(path):
        for fn in filenames:
            if re_fname.match(fn):
                yield os.path.join(dirpath, fn)


def choose_files_to_remove(files, now=None):
    """
    Given a sequence of backup filenames, yield the ones that can be removed
    """
    if now is None:
        now = datetime.date.today()

    by_type = defaultdict(list)
    for fname in files:
        basename = os.path.basename(fname)
        if not re_fname.match(basename):
            continue
        date = datetime.datetime.strptime(basename[:8], "%Y%m%d").date()

        if basename.endswith(".gz"):
            name, ext = os.path.splitext(basename[:-3])
        else:
            name, ext = os.path.splitext(basename)
        dirname = os.path.basename(os.path.dirname(fname))
        by_type[(dirname, ext)].append((date, fname))

    for names in by_type.values():
        names.sort(reverse=True)

        last = now
        for date, fname in names:
            age = now - date
            age_from_last = last - date

            # Keep all files in the last week
            if age <= datetime.timedelta(days=7):
                last = date
                continue

            # Keep no more than one file per week in the last 6 months
            if age <= datetime.timedelta(days=6 * 30) and age_from_last >= datetime.timedelta(days=7):
                last = date
                continue

            # Keep one file per month in the last 2 years
            if age <= datetime.timedelta(days=365 * 2) and age_from_last >= datetime.timedelta(days=30):
                last = date
                continue

            # Keep one file per year otherwise
            if age > datetime.timedelta(days=365 * 2) and age_from_last >= datetime.timedelta(days=365):
                last = date
                continue

            yield fname
