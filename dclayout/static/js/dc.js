(function() {
"use strict";

// See https://webdesign.tutsplus.com/tutorials/how-to-make-the-bootstrap-navbar-dropdown-work-on-hover--cms-33840
class NavbarHover
{
    constructor(dropdown)
    {
        this.dropdown = $(dropdown);
        this.dropdown_toggle = this.dropdown.find(".dropdown-toggle");
        this.dropdown_menu = this.dropdown.find(".dropdown-menu");
        $(window).on("resize", evt => { this.refresh(); });
        this.refresh();
    }

    hover_in(evt)
    {
        this.dropdown.addClass("show");
        this.dropdown_toggle.attr("aria-expanded", "true");
        this.dropdown_menu.addClass("show");
    }

    hover_out(evt)
    {
        this.dropdown.removeClass("show");
        this.dropdown_toggle.attr("aria-expanded", "false");
        this.dropdown_menu.removeClass("show");
    }

    refresh()
    {
        if (window.matchMedia("(min-width: 768px)").matches)
        {
            this.dropdown.hover(
                evt => { this.hover_in(evt); },
                evt => { this.hover_out(evt); });
        } else {
            this.dropdown.off("mouseenter mouseleave");
        }
    }

    static install()
    {
        $("#main-navbar1").find(".dropdown").each((idx, el) => {
            new NavbarHover(el);
        });
    }
}

// Install fingerprint easy-copy-paste behaviour
function setup_fpr()
{
    $("span.fpr").each(function(idx, el) {
        var el = $(el);
        var joined = $("<input>").attr("type", "text").attr("size", "40").val(el.text().replace(/ /g,""));
        el.after(joined);
        joined.hide();
        el.click(function(ev) {
            el.hide();
            joined.show();
            joined.focus();
        });
        joined.focusout(function(ev) {
            el.show();
            joined.hide();
        });
    });
}

function setup_tablesorter()
{
    for (let el of document.getElementsByClassName("tablesorter"))
    {
        let table = $(el).DataTable({
            paging: false,
        });
    }
}

function main()
{
    setup_tablesorter();
    setup_fpr();

    NavbarHover.install();

    // Initialize message notification
    $(".toast").toast({
        delay: 5000,
    }).toast('show');
}

document.addEventListener("DOMContentLoaded", evt => { main(); });

})();
